const express = require('express');
const router = express.Router();

// Require the controllers
const encomendaController = require('../controllers/encomendaController');

// m�todos de cria��o
router.post('/', encomendaController.criar_uma_encomenda);

// m�todos de procura/busca
router.get('/', encomendaController.obter_todas_encomendas);
router.get('/buscarEncomendas/:idCliente/:estadoEncomenda', encomendaController.obter_encomenda_cliente_estado);
router.get('/:id', encomendaController.obter_uma_encomenda);
router.get('/:id/itens', encomendaController.itens_produto_encomendado);
router.get('/:idEncomenda/itens/:idItemProduto', encomendaController.item_produto_encomendado);

// m�todos de atualiza��o
router.put('/estado/:idEncomenda', encomendaController.atualizar_estado_encomenda);
router.put('/:id', encomendaController.atualizar_uma_encomenda);

// m�todos de remo��o
router.delete('/:id', encomendaController.apagar_uma_encomenda);
module.exports = router;