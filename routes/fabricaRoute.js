const express = require('express');
const router = express.Router();
// o require basicamente � como um criador de um objecto
const fabricaController = require('../controllers/fabricaController');

// m�todos de procura/busca
router.post('/', fabricaController.criar_fabrica);
// router.put('/:id', fabricaController.atualizar_cidade);
// router.delete('/:id', fabricaController.apagar_cidade);
// router.get('/', fabricaController.obter_cidades);
// router.get('/:id', fabricaController.obter_cidade);

module.exports = router;
