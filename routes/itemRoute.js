const express = require('express');
const router = express.Router();
// o require basicamente � como um criador de um objecto
const itemController = require('../controllers/itemController');

// m�todos de procura/busca
router.get('/itensNaoEncomendados', itemController.itens_nao_encomendados);
router.get('/', itemController.obter_itens);
router.get('/:id', itemController.obter_item);
router.get('/itensProduto/:idProduto', itemController.itens_produto);
router.get('/podeApagar/produto/:idProduto', itemController.podeApagarProduto);
router.get('/podeEditar/produto/:idProduto', itemController.dimensoesItensDeProduto);
router.get('/podeApagar/material/:idMaterial', itemController.podeApagarMaterial);
router.get('/podeApagar/acabamento/:idAcabamento', itemController.podeApagarAcabamento);


//m�todos de cria��o
router.post('/', itemController.criar_item);

//métodos de atualização
router.put('/:id',itemController.atualizar_item);
//m�todos de remo��o
router.delete('/:id', itemController.apagar_item);

module.exports = router;
