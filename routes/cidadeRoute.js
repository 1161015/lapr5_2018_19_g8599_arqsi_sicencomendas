const express = require('express');
const router = express.Router();
// o require basicamente � como um criador de um objecto
const cidadeController = require('../controllers/cidadeController');

// m�todos de procura/busca
router.post('/', cidadeController.criar_cidade);
router.put('/:id', cidadeController.atualizar_cidade);
router.delete('/:id', cidadeController.apagar_cidade);
router.get('/', cidadeController.obter_cidades);
router.get('/:id', cidadeController.obter_cidade);

module.exports = router;
