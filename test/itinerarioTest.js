const assert = require('chai').assert;
const Fabrica = require('../models/fabrica');
const Itinerario = require('../models/itinerario');
const itinerarioDTO = require('../dtos/itinerarioDTO');
const Cidade = require('../models/cidade');
const fabricaDTO = require('../dtos/fabricaDTO');

const cidade1 = new Cidade({
    Nome: "Porto",
    Latitude: 10,
    Longitude: 10
});

const fabrica1 = new Fabrica(
    {
        Cidade: cidade1
    }
);

const itinerario1 = new Itinerario(
    {
        Percurso: null,
        Fabrica: fabrica1,
        Data: Date.now 
    }
)

describe('Itinerario', function () {
    it('Conversão DTO itinerario', function () {
        let dto = itinerario1.toDTO();
        let percurso = null;
        let esperado = new itinerarioDTO(itinerario1._id, percurso, fabrica1.toDTO(), itinerario1.Data);
        assert.deepEqual(dto, esperado);
    });
})