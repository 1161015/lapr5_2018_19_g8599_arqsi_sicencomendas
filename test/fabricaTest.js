const assert = require('chai').assert;
const Fabrica = require('../models/fabrica');
const Cidade = require('../models/cidade');
const fabricaDTO = require('../dtos/fabricaDTO');

const cidade1 = new Cidade({
    Nome: "Porto",
    Latitude: 10,
    Longitude: 10
});

const fabrica1 = new Fabrica(
    {
        Cidade: cidade1
    }
);

describe('Fabrica', function () {
    it('Conversão DTO fabrica', function () {
        let dto = fabrica1.toDTO();
        let esperado = new fabricaDTO(fabrica1._id, cidade1.toDTO());
        assert.deepEqual(dto, esperado);
    });
})