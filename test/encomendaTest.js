const assert = require('chai').assert;
const encomenda = require('../models/encomenda');
const encomendaDTO = require('../dtos/encomendaDTO');
const item = require('../models/item');
const Fabrica = require('../models/fabrica');
const Cidade = require('../models/cidade');
const fabricaDTO = require('../dtos/fabricaDTO');

const item1 = new item(
    {
        id_produto: 5,
        id_material: 4,
        id_acabamento: 3,
        IdUnidade: 1,
        ItensFilhos: [],
        Largura: 10,
        Altura: 45,
        Profundidade: 78
    }
);

const item2 = new item(
    {
        id_produto: 15,
        id_material: 40,
        id_acabamento: 41,
        IdUnidade: 2,
        ItensFilhos: [item1],
        Largura: 412,
        Altura: 457,
        Profundidade: 781
    }
);

const cidade1 = new Cidade({
    Nome: "Porto",
    Latitude: 10,
    Longitude: 10
});

const fabrica1 = new Fabrica(
    {
        Cidade: cidade1
    }
);

const encomenda2 = new encomenda({
    Itens: [],
    Data: Date.now,
    Morada: cidade1,
    idCliente: 1,
    FabricaAtribuida: fabrica1,
    EstadoEncomenda: "Submetida",
    Preco: 20
});

const encomenda1 = new encomenda({
    Itens: [item1, item2],
    Data: Date.now,
    Morada: cidade1,
    idCliente: 1,
    FabricaAtribuida: fabrica1,
    EstadoEncomenda: "Submetida",
    Preco: 20
});

describe('Encomenda', function () {
    it('ConversÃ£o DTO sem itens', function () {
        let encomendaDTOResultado = encomenda2.toDTO();
        let encomendaDTOEsperada = new encomendaDTO(encomenda2._id, [], encomenda2.Data, encomenda2.Morada.toDTO(), encomenda2.idCliente, encomenda1.Preco);
        assert.deepEqual(encomendaDTOResultado, encomendaDTOEsperada);
    })

    it('ConversÃ£o DTO encomenda com itens', function () {
        let encomendaDTOResultado = encomenda1.toDTO();
        let itensDTOsEsperados = [];
        let item1DTOEsperado = item1.toDTO();
        let item2DTOEsperado = item2.toDTO();
        itensDTOsEsperados.push(item1DTOEsperado);
        itensDTOsEsperados.push(item2DTOEsperado);
        let encomendaDTOEsperada = new encomendaDTO(encomenda1._id, itensDTOsEsperados, encomenda1.Data, encomenda1.Morada.toDTO(), encomenda1.idCliente, encomenda1.Preco);
        assert.deepEqual(encomendaDTOResultado, encomendaDTOEsperada);
    })


});