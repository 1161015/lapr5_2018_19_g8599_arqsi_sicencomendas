const assert = require('chai').assert;
const item = require('../models/item');
const service = require('../services/itemService');


const pai = new item(
    {
        id_produto: 5,
        id_material: 4,
        id_acabamento: 3,
        IdUnidade: 1,
        ItensFilhos: [],
        Largura: 600,
        Altura: 600,
        Profundidade: 600
    }
);

const filho1 = new item({
    id_produto: 10,
    id_material: 5,
    id_acabamento: 8,
    IdUnidade: 2,
    ItensFilhos: [],
    Largura: 500,
    Altura: 500,
    Profundidade: 500
});

const filho2 = new item(
    {
        id_produto: 5,
        id_material: 4,
        id_acabamento: 3,
        IdUnidade: 1,
        ItensFilhos: [],
        Largura: 600,
        Altura: 600,
        Profundidade: 600
    }
);

const filho3 = new item(
    {
        id_produto: 5,
        id_material: 4,
        id_acabamento: 3,
        IdUnidade: 1,
        ItensFilhos: [],
        Largura: 605,
        Altura: 605,
        Profundidade: 605
    }
);

const filho4 = new item(
    {
        id_produto: 5,
        id_material: 4,
        id_acabamento: 3,
        IdUnidade: 1,
        ItensFilhos: [],
        Largura: 88,
        Altura: 88,
        Profundidade: 88
    }
);

const filho5 = new item(
    {
        id_produto: 5,
        id_material: 4,
        id_acabamento: 3,
        IdUnidade: 1,
        ItensFilhos: [],
        Largura: 500,
        Altura: 500,
        Profundidade: 500
    }
);

const filho6 = new item(
    {
        id_produto: 5,
        id_material: 4,
        id_acabamento: 3,
        IdUnidade: 1,
        ItensFilhos: [],
        Largura: 350,
        Altura: 350,
        Profundidade: 350
    }
);

const filho7 = new item(
    {
        id_produto: 5,
        id_material: 4,
        id_acabamento: 3,
        IdUnidade: 1,
        ItensFilhos: [],
        Largura: 400,
        Altura: 400,
        Profundidade: 400
    }
);

describe('Item', function () {

    it('Testar inserir 1 item com dimensões menor do que o pai.', async function () {
        let servicoItem = new service();
        let resultado = await servicoItem.adicionarItemFilho(pai, filho1);
        let resultadoEsperado = true;
        assert.equal(resultado, resultadoEsperado);
    });

    it('Testar inserir 1 item com dimensões iguais ás do pai.', async function () {
        let servicoItem = new service();
        let resultado = await servicoItem.adicionarItemFilho(pai, filho2);
        let resultadoEsperado = false;
        assert.equal(resultado, resultadoEsperado);
    });

    it('Testar inserir 1 item com dimensões superiores ás do pai.', async function () {
        let servicoItem = new service();
        let resultado = await servicoItem.adicionarItemFilho(pai, filho3);
        let resultadoEsperado = false;
        assert.equal(resultado, resultadoEsperado);
    });

    it('Testar inserir 2 item com dimensões inferiores ás do pai.', async function () {
        let servicoItem = new service();
        pai.ItensFilhos = [];
        let resultado1 = await servicoItem.adicionarItemFilho(pai, filho4);
        let resultado2 = await servicoItem.adicionarItemFilho(pai, filho5);
        let resultadoEsperado1 = true;
        let resultadoEsperado2 = true;
        assert.equal(resultado1, resultadoEsperado1);
        assert.equal(resultado2, resultadoEsperado2);
    });

    it('Testar inserir 2 item com dimensões inferiores ás do pai. (no entanto a combinação não cabe no pai) ', async function () {
        let servicoItem = new service();
        pai.ItensFilhos = [];
        let resultado1 = await servicoItem.adicionarItemFilho(pai, filho6);
        let resultado2 = await servicoItem.adicionarItemFilho(pai, filho7);
        let resultadoEsperado1 = true;
        let resultadoEsperado2 = false;
        assert.equal(resultado1, resultadoEsperado1);
        assert.equal(resultado2, resultadoEsperado2);
    });




})

