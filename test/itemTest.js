const assert = require('chai').assert;
const item = require('../models/item');
const itemDTO = require('../dtos/itemDTO');
const item1 = new item(
    {
        id_produto: 5,
        id_material: 4,
        id_acabamento: 3,
        ItensFilhos: [],
        Largura: 10,
        Altura: 45,
        Profundidade: 78
    }
);

const item3 = new item({
    id_produto: 10,
    id_material: 5,
    id_acabamento: 8,
    ItensFilhos: [],
    Largura: 100,
    Altura: 47,
    Profundidade: 42
});

const item2 = new item(
    {
        id_produto: 5,
        id_material: 4,
        id_acabamento: 3,
        ItensFilhos: [item3],
        Largura: 10,
        Altura: 45,
        Profundidade: 78
    }
);

describe('Item', function () {
    it('Conversão DTO sem item Filho', function () {
        let itemAtualDTO = item1.toDTO();
        let itemDTOEsperado = new itemDTO(item1._id, 5, 4, 3, [], 45, 10, 78);
        assert.deepEqual(itemAtualDTO, itemDTOEsperado);
    });
    it('Conversão DTO com filhos ', function () {
        let itemAtualDTO = item2.toDTO();
        let itemDTOFilho = new itemDTO(item3._id, 10, 5, 8, [], 47, 100, 42);
        let itemDTOEsperado = new itemDTO(item2._id, 5, 4, 3, [itemDTOFilho], 45, 10, 78);
        assert.deepEqual(itemAtualDTO, itemDTOEsperado);
    });
})
