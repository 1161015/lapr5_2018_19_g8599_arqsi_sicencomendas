class encomendaDTO {
	constructor(idEncomenda, itens, dataEncomenda, morada, idCliente, preco) {
		this.idEncomenda = idEncomenda;
		this.itens = itens;
		this.dataEncomenda = dataEncomenda;
		this.morada = morada;
		this.idCliente = idCliente;
		this.preco = preco;
	}
}

module.exports = encomendaDTO;