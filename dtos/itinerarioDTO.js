class itinerarioDTO {
	constructor(idItinerario, Percurso, Fabrica, Data) {
        this.idItinerario = idItinerario;
        this.Percurso = Percurso;
        this.Fabrica = Fabrica;
        this.Data = Data;
	}
}

module.exports = itinerarioDTO;