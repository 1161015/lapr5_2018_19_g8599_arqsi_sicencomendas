class cidadeDTO {
	constructor(idCidade, Nome, Latitude, Longitude) {
		this.idCidade = idCidade;
		this.Nome = Nome;
		this.Latitude = Latitude;
		this.Longitude = Longitude;
	}
}

module.exports = cidadeDTO;