class itemDTO {
	constructor(idItem,idProduto, idMaterial,idAcabamento, itensFilhos, altura, largura, profundidade,preco) {
		this.idItem = idItem;
		this.idProduto = idProduto;
		this.idMaterial = idMaterial;
		this.idAcabamento = idAcabamento;
		this.itensFilhos = itensFilhos;
		this.altura = altura;
		this.largura = largura;
		this.profundidade = profundidade;
		this.preco = preco;
	}
}

module.exports = itemDTO;