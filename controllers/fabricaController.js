const fabrica = require('../models/fabrica');

exports.criar_fabrica = function (req, res) {
    let nova_fabrica = new fabrica(
        {
            Cidade: req.body.Cidade,
        });

    nova_fabrica.save(function (err) {
        if (err) {
            res.send("Erro ao criar - verificar integridade dos dados!");
            return;
        }
        res.send(nova_fabrica);
    });

}