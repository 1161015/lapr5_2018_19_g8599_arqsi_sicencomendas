const encomenda = require('../models/encomenda');
const item = require('../models/item');
const encomendaService = require('../services/encomendaService');
var service = new encomendaService();
var sync = require('sync');
var tarefa = require('node-schedule');
var estadosDasEncomendas = require('../models/encomenda').estados;

const TEMPO_ESPERA = 5;


exports.atualizar_estado_encomenda = async function (req, res) {
	let encomendaDesejada = await encomenda.findOne({ _id: req.params.idEncomenda }).exec();
	if (encomendaDesejada.EstadoEncomenda != estadosDasEncomendas.ENTREGUE) {
		return res.status(400).send({
			message: 'Apenas é possível alterar a encomenda para rececionada, se esta se encontrar no estado de entregue'
		});
	}
	encomenda.findByIdAndUpdate(req.params.idEncomenda,
		{
			EstadoEncomenda: estadosDasEncomendas.RECECIONADA
		}, function (err) {
			if (err) {
				res.status(400).send({
					message: err
				});
			} else {
				res.status(204).send({
					message: 'Estado alterado com sucesso'
				});
			}
		});
}

exports.obter_encomenda_cliente_estado = function (req, res) {
	encomenda.find({ IdCliente: req.params.idCliente, EstadoEncomenda: req.params.estadoEncomenda }, function (err, encomendas) {
		if (err) {
			res.send("Erro ao obter encomenda");
		} else {
			let i;
			let encomendasDTO = [];
			for (i = 0; i < encomendas.length; i++) {
				encomendasDTO.push(encomendas[i].toDTO());
			}
			res.send(encomendasDTO);
		}
	});
}

exports.criar_uma_encomenda = async function (req, res) {
	let i;
	let promessas = [];
	let todosItensEncomenda = [];
	let k;
	for (k = 0; k < req.body.Itens.length; k++) {
		let idItemAtual = req.body.Itens[k];
		let itemAtual = await item.findOne({ _id: idItemAtual }).exec();
		service.preencherTodosItensEncomendaRecursivamente(todosItensEncomenda, itemAtual);
	}

	for (i = 0; i < todosItensEncomenda.length; i++) {
		promessas.push(service.validarObrigatoriedade(todosItensEncomenda[i]._id));
		promessas.push(service.validarTaxaOcupacao(todosItensEncomenda[i]._id));
		promessas.push(service.validarMaterialAcabamento(todosItensEncomenda[i]._id));
	}
	Promise.all(promessas).then(function (result) {
		let new_encomenda = new encomenda(
			{
				Itens: req.body.Itens,
				Data: req.body.Data,
				Morada: req.body.Morada,
				IdCliente: req.body.IdCliente,
				Preco: req.body.Preco
			}
		);
		new_encomenda.save(function (err) {
			if (err) {
				res.send("Erro ao criar - verificar integridade dos dados!");
			}
			let dataAtual = new Date();
			let dataTarefa = new Date(dataAtual.getTime() + TEMPO_ESPERA * 60000);
			tarefa.scheduleJob(dataTarefa, function () {
				service.validarEncomenda(new_encomenda);
			});
			res.send(new_encomenda._id);
		});
	}).catch(result => res.status(400).send({
		message: result
	}));


};


exports.obter_todas_encomendas = function (req, res) {
	encomenda.find({}, function (err, encomendas) {
		if (err) {
			res.send("Erro ao obter encomendas");
		} else {
			let encomendasDTO = []; // array vazio
			let i;
			for (i = 0; i < encomendas.length; i++) {
				encomendasDTO.push(encomendas[i].toDTO());
			}
			res.send(encomendasDTO);
		}
	});

};

exports.obter_uma_encomenda = function (req, res) {
	encomenda.findById(req.params.id, function (err, enc) {
		if (err) {
			res.send("Erro ao obter encomenda");
		} else {
			let encDTO = enc.toDTO();
			res.send(encDTO);
		}
	});
};

exports.itens_produto_encomendado = function (req, res) {
	encomenda.findById(req.params.id, function (err, enc) {
		if (err) res.send("Erro ao obter itens");
		let itensDTOs = [];
		let i;
		for (i = 0; i < enc.Itens.length; i++) {
			itensDTOs.push(enc.Itens[i].toDTO());
		}
		res.send(itensDTOs);
	});
}

exports.item_produto_encomendado = function (req, res) {
	encomenda.findById(req.params.idEncomenda, function (err, enc) {
		if (err) res.send("Erro ao obter item");
		let i;
		for (i = 0; i < enc.Itens.length; i++) {
			if (enc.Itens[i]._id.equals(req.params.idItemProduto)) {
				let itemDTO = enc.Itens[i].toDTO();
				return res.send(itemDTO);
			}
		}
		res.status(404).send({ message: "A encomenda existe por�m o item n�o existe." });
	});
};

exports.atualizar_uma_encomenda = async function (req, res) {
	let i;
	let promessas = [];
	let todosItensEncomenda = [];
	let k;
	for (k = 0; k < req.body.Itens.length; k++) {
		let idItemAtual = req.body.Itens[k];
		let itemAtual = await item.findOne({ _id: idItemAtual }).exec();
		service.preencherTodosItensEncomendaRecursivamente(todosItensEncomenda, itemAtual);
	}

	for (i = 0; i < todosItensEncomenda.length; i++) {
		promessas.push(service.validarObrigatoriedade(todosItensEncomenda[i]._id));
		promessas.push(service.validarTaxaOcupacao(todosItensEncomenda[i]._id));
		promessas.push(service.validarMaterialAcabamento(todosItensEncomenda[i]._id));
	}
	Promise.all(promessas).then(function (result) {
		encomenda.findByIdAndUpdate(req.params.id,
			{
				$set: req.body
			}, function (err) {
				if (err)
					res.send("Erro ao atualizar - verificar integridade dos dados!");
				res.send("Encomenda atualizada!");
			});
	}).catch(result => res.status(400).send({
		message: result
	}));
};

exports.apagar_uma_encomenda = function (req, res) {
	encomenda.findByIdAndRemove(req.params.id, function (err) {
		if (err)
			res.send("Erro ao apagar");
		res.send("Encomenda removida com sucesso");
	});
};


percorreArvore = function (item) {
	let i;
	for (i = 0; item.ItensFilhos.length; i++) {
		if (!percorreArvore(item.ItensFilhos[i])) return false;
	}
	return servico.validarTaxaOcupacao(item);
}
