const cidade = require('../models/cidade');

exports.criar_cidade = function (req, res) {
	let nova_cidade = new cidade(
		{
			Nome: req.body.Nome,
			Latitude: req.body.Latitude,
			Longitude: req.body.Longitude
		});

	nova_cidade.save(function (err) {
		if (err) {
			res.send("Erro ao criar - verificar integridade dos dados!");
		}
		res.send(nova_cidade);
	});

}

exports.apagar_cidade = async function (req, res) {
	cidade.findByIdAndRemove(req.params.id, async function (err, resultado) {
		if (err) {
			res.send("Erro inesperado ao apagar a cidade - por favor verifique integridade dos dados");
		} else {
			res.send(resultado);
		}
	});
};

exports.atualizar_cidade = function (req, res) {
		cidade.findByIdAndUpdate(req.params.id, {
			$set: req.body
		}, function (err) {
			if (err) res.send("Erro ao atualizar cidade - verificar integridade dos dados!");
			res.send('Cidade atualizada com sucesso!');
		})
}

exports.obter_cidades = function (req, res) {
	cidade.find({}, function (err, cidades) {
		if (err) {
			res.send("Erro ao obter itens");
		} else {
			let cidadesDTOs = [];
			let i;
			for (i = 0; i < cidades.length; i++) {
				cidadesDTOs.push(cidades[i].toDTO());
			}
			res.send(cidadesDTOs);
		}
	});
};

exports.obter_cidade = function (req, res) {
	cidade.findById(req.params.id, function (err, cidade_obtida) {
		if (err) {
			res.send("Erro ao obter cidade com o id inserido - por favor verifique integridade dos dados")
		} else {
			let cidadeDTO = cidade_obtida.toDTO();
			res.send(cidadeDTO);
		}
	});
};

