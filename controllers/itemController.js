const item = require('../models/item');
const itemService = require('../services/itemService');
const encomendaService = require('../services/encomendaService');
var servico = new itemService();
var encomenda = new encomendaService();


exports.obter_itens = function (req, res) {
	item.find({}, function (err, itens) {
		if (err) {
			res.send("Erro ao obter itens");
		} else {
			let itensDTOs = [];
			let i;
			for (i = 0; i < itens.length; i++) {
				itensDTOs.push(itens[i].toDTO());
			}
			res.send(itensDTOs);
		}
	});
};

exports.podeApagarProduto = function (req, res) {
	item.find({}, function (err, itens) {
		if (err) {
			res.send("Erro ao obter itens");
		} else {
			for (i = 0; i < itens.length; i++) {
				if (itens[i].id_produto == req.params.idProduto) return res.status(200).send(false);
			}
			return res.status(200).send(true);
		}
	});
};

exports.dimensoesItensDeProduto = async function (req, res) {
	let itensDeProduto = await item.find({ id_produto: req.params.idProduto }).exec();
	let listaTriplos = [];
	let i;
	for(i=0; i<itensDeProduto.length; i++){
		listaTriplos.push({
			"Altura" : itensDeProduto[i].Altura,
			"Largura" : itensDeProduto[i].Largura,
			"Profundidade" : itensDeProduto[i].Profundidade,
		});
	}
	res.status(200).send(listaTriplos);
}

exports.podeApagarMaterial = function (req, res) {
	item.find({}, function (err, itens) {
		if (err) {
			res.send("Erro ao obter itens");
		} else {
			for (i = 0; i < itens.length; i++) {
				if (itens[i].id_material == req.params.idMaterial) return res.status(200).send(false);
			}
			return res.status(200).send(true);
		}
	});
};

exports.podeApagarAcabamento = function (req, res) {
	item.find({}, function (err, itens) {
		if (err) {
			res.send("Erro ao obter itens");
		} else {
			for (i = 0; i < itens.length; i++) {
				if (itens[i].id_acabamento == req.params.idAcabamento) return res.status(200).send(false);
			}
			return res.status(200).send(true);
		}
	});
};

exports.obter_item = function (req, res) {
	item.findById(req.params.id, function (err, item_obtido) {
		if (err) {
			res.send("Erro ao obter item com o id inserido - por favor verifique integridade dos dados")
		} else {
			let itemDTO = item_obtido.toDTO();
			res.send(itemDTO);
		}
	});
};




exports.itens_nao_encomendados = async function (_, res) {
	let listaItens = await item.find({}).exec();
	let i;
	let retorno = [];
	for (i = 0; i < listaItens.length; i++) {
		let existeEmEncomenda = await encomenda.itemPresenteEncomenda(listaItens[i]._id);
		if (!existeEmEncomenda) {
			retorno.push(listaItens[i].toDTO());
		}
	}
	return res.send(retorno);
}

exports.itens_produto = async function (req, res) {
	let listaItens = await item.find({}).exec();
	let i;
	let retorno = [];
	for (i = 0; i < listaItens.length; i++) {
		if (listaItens[i].id_produto == req.params.idProduto) {
			retorno.push(listaItens[i].toDTO());
		}
	}
	return res.send(retorno);
}


exports.apagar_item = async function (req, res) {

	let existeEncomenda = await encomenda.itemPresenteEncomenda(req.params.id);
	if (existeEncomenda) {
		return res.send('O item já se encontra encomendado, impossível de ser apagado.');
	}

	item.findByIdAndRemove(req.params.id, async function (err, resultado) {
		if (err) {
			res.send("Erro inesperado ao apagar o item - por favor verifique integridade dos dados");
		} else {
			await servico.apagarReferencias(req.params.id);
			res.send(resultado);
		}
	});
};

exports.atualizar_item = async function (req, res) {
	let i;
	let fakeItemPai = new item({
		ItensFilhos: [],
		Altura: req.body.Altura,
		Largura: req.body.Largura,
		Profundidade: req.body.Profundidade
	});
	for (i = 0; i < req.body.ItensFilhos.length; i++) {
		let itemFilho = await item.findOne({ _id: req.body.ItensFilhos[i] }).exec();
		let cabe = await servico.adicionarItemFilho(fakeItemPai, itemFilho);
		if (!cabe) {
			return res.status(400).send({
				message: 'Não existe uma combinação possível onde os filhos encaixem no pai'
			});
		}
	}
	let itensPais = await servico.buscarTodosItensPai(req.params.id);   // simular agora um put do item pai
	// com as novas dimensões do item filho
	// basta um deles não caber no pai, para falhar a atualização

	let aux = String(req.params.id);
	let j;
	for (j = 0; j < itensPais.length; j++) {
		let itemPaiAtual = itensPais[j];
		let fakeItemPaiAtual = new item({
			ItensFilhos: [],
			Altura: itemPaiAtual.Altura,
			Largura: itemPaiAtual.Largura,
			Profundidade: itemPaiAtual.Profundidade
		});
		let k;
		for (k = 0; k < itemPaiAtual.ItensFilhos.length; k++) {
			let itemFilhoAtual = itemPaiAtual.ItensFilhos[k];
			let aux2 = String(itemFilhoAtual._id);
			if (aux === aux2) { // se for o próprio item filho, então colocar as dimensões novas, que vieram pelo pedido PUT
				itemFilhoAtual.Altura = req.body.Altura;
				itemFilhoAtual.Largura = req.body.Largura;
				itemFilhoAtual.Profundidade = req.body.Profundidade;
			}
			let cabe = await servico.adicionarItemFilho(fakeItemPaiAtual, itemFilhoAtual);
			if (!cabe) {
				return res.status(400).send({
					message: 'Impossível atualizar, porque iria causar dimensões incompatíveis em pelo menos um dos seus itens pai.'
				});
			}

		}
	}

	let validarProduto = servico.validarProdutoId(req.body.id_produto);
	let validarDimensoes = servico.validarDimensoes(req.body.id_produto, req.body.Altura, req.body.Largura, req.body.Profundidade);
	let validarMA = servico.validarMaterialAcabamento(req.body.id_produto, req.body.id_material, req.body.id_acabamento);
	let validarItensFilhos = servico.validarItensFilhos(req.body.id_produto, req.body.ItensFilhos);
	Promise.all([validarProduto, validarItensFilhos, validarDimensoes, validarMA]).then(function (_) {
		item.findByIdAndUpdate(req.params.id, {
			$set: req.body
		}, function (err) {
			if (err) res.send("Erro ao atualizar - verificar integridade dos dados!");
			res.status(204).send({
				message: 'Item atualizado com sucesso!'
			});
		})
	}).catch(result => res.status(400).send({
		message: result
	}));
}

exports.criar_item = async function (req, res) {
	let i;
	let fakeItemPai = new item({
		ItensFilhos: [],
		Altura: req.body.Altura,
		Largura: req.body.Largura,
		Profundidade: req.body.Profundidade
	});
	for (i = 0; i < req.body.ItensFilhos.length; i++) {
		let itemFilho = await item.findOne({ _id: req.body.ItensFilhos[i] }).exec();
		let cabe = await servico.adicionarItemFilho(fakeItemPai, itemFilho);
		if (!cabe) {
			return res.status(400).send({
				message: 'Não existe uma combinação possível onde os filhos encaixem no pai'
			});
		}
	}
	let validarProduto = servico.validarProdutoId(req.body.id_produto);
	let validarDimensoes = servico.validarDimensoes(req.body.id_produto, req.body.Altura, req.body.Largura, req.body.Profundidade);
	let validarMA = servico.validarMaterialAcabamento(req.body.id_produto, req.body.id_material, req.body.id_acabamento);
	let validarItensFilhos = servico.validarItensFilhos(req.body.id_produto, req.body.ItensFilhos);
	Promise.all([validarProduto, validarItensFilhos, validarDimensoes, validarMA]).then(function (_) {

		let novo_item = new item(
			{
				id_produto: req.body.id_produto,
				id_material: req.body.id_material,
				id_acabamento: req.body.id_acabamento,
				ItensFilhos: req.body.ItensFilhos,
				Altura: req.body.Altura,
				Largura: req.body.Largura,
				Profundidade: req.body.Profundidade,
				Preco: req.body.Preco
			});
		novo_item.save(function (err) {
			if (err) {
				res.send("Erro ao criar - verificar integridade dos dados!");
			}

			res.send(novo_item._id);
		});
	}).catch(result => res.status(400).send({
		message: result
	}));

};

