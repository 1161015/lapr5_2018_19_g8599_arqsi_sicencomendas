var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var idvalidator = require('mongoose-id-validator');
const itinerarioDTO = require('../dtos/itinerarioDTO');


var ItinerarioSchema = new Schema({
    Percurso: [{ type: mongoose.Schema.Types.ObjectId, autopopulate: true }],
    Fabrica: { type: mongoose.Schema.Types.ObjectId, ref: 'Fabrica', autopopulate: true },
    Data: { type: Date, default: Date.now }
});

ItinerarioSchema.methods.toDTO = function () {

    let fabricaDTO = this.Fabrica.toDTO();
    let percurso = this.Percurso;
    let data = this.Data;

    return new itinerarioDTO(this._id, percurso, fabricaDTO, data);
}

ItinerarioSchema.plugin(idvalidator);

ItinerarioSchema.plugin(require('mongoose-autopopulate'));

module.exports = mongoose.model('Itinerario', ItinerarioSchema);