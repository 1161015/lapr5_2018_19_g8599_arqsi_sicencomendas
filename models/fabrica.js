var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var idvalidator = require('mongoose-id-validator');
const fabricaDTO = require('../dtos/fabricaDTO');


var FabricaSchema = new Schema({
    Cidade: {type: mongoose.Schema.Types.ObjectId, ref: 'Cidade', autopopulate:true}
});

FabricaSchema.methods.toDTO = function () {
    
	let cidadeDTO = this.Cidade.toDTO();
    return new fabricaDTO(this._id, cidadeDTO);
}

FabricaSchema.plugin(idvalidator);

FabricaSchema.plugin(require('mongoose-autopopulate'));

module.exports = mongoose.model('Fabrica', FabricaSchema);