var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var idvalidator = require('mongoose-id-validator');
const cidadeDTO = require('../dtos/cidadeDTO');


var CidadeSchema = new Schema({
    Nome: { type: String, required: 'Nome Obrigatório' },
    Latitude: { type: Number, required: 'Latitude Obrigatória' }, // não é preciso ter mais do que 3 casas decimais
    Longitude: { type: Number, required: 'Longitude Obrigatória' }
});

CidadeSchema.methods.toDTO = function () {
    return new cidadeDTO(this._id, this.Nome, this.Latitude, this.Longitude);
}

CidadeSchema.plugin(idvalidator);

CidadeSchema.plugin(require('mongoose-autopopulate'));

module.exports = mongoose.model('Cidade', CidadeSchema);