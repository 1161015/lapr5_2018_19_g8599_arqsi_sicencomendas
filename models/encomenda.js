var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var idvalidator = require('mongoose-id-validator');
const encomendaDTO = require('../dtos/encomendaDTO');

// https://www.npmjs.com/package/node-schedule
var estadoEncomenda = {
	SUBMETIDA: "Submetida",
	VALIDADA: "Validada",
	ASSIGNADA: "Assignada",
	EM_PRODUCAO: "Em produção",
	EM_EMBALAMENTO: "Em embalamento",
	PRONTA_EXPEDIR: "Pronta expedir",
	EXPEDIDA: "Expedida",
	ENTREGUE: "Entregue",
	RECECIONADA: "Rececionada" // na ui do angular um botão
};

var EncomendaSchema = new Schema({
	Itens: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Item', autopopulate: true }],
	Data: { type: Date, default: Date.now },
	Morada: { type: mongoose.Schema.Types.ObjectId, ref: 'Cidade', autopopulate: true },
	IdCliente: { type: Number, required: 'Referência ao cliente necessária' }, // vai ser preenchido pelo angular
	FabricaAtribuida: { type: mongoose.Schema.Types.ObjectId, ref: 'Fabrica' }, // pedido HTTP ao Sic_Nav
	EstadoEncomenda: { type: estadoEncomenda, default: estadoEncomenda.SUBMETIDA },
	Preco: { type: Number, required: 'Preço total da encomenda necessário' }
});

EncomendaSchema.methods.toDTO = function () {
	var itensDTOs = []; // basicamente cria um array vazio
	let i;
	for (i = 0; i < this.Itens.length; i++) {
		itensDTOs.push(this.Itens[i].toDTO());
	}
	let cidadeDTO = this.Morada.toDTO();

	return new encomendaDTO(this._id, itensDTOs, this.Data, cidadeDTO, this.IdCliente, this.Preco);
}

EncomendaSchema.plugin(idvalidator);

EncomendaSchema.plugin(require('mongoose-autopopulate'));

module.exports = mongoose.model('Encomenda', EncomendaSchema);

module.exports.estados = estadoEncomenda;