var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var idvalidator = require('mongoose-id-validator');
const itemDTO = require('../dtos/itemDTO');

var ItemSchema = new Schema({
	id_produto: {
		type: Number,
		required: 'Referência a Produto Obrigatória'
	},
	id_material: {
		type: Number,
		required: 'Referência a Material Obrigatória'
	},
	id_acabamento: {
		type: Number,
		required: 'Referência a Acabamento Obrigatória'
	},
	ItensFilhos: [{
		type: mongoose.Schema.Types.ObjectId, ref: 'Item', autopopulate: true
	}],
	Altura: {
		type: Number,
		required: 'Altura Obrigatória'
	},
	Largura: {
		type: Number,
		required: 'Largura Obrigatória'
	},
	Profundidade: {
		type: Number,
		required: 'Profundidade Obrigatória'
	},
	Preco: {
		type: Number,
		required: 'Preço Obrigatório'
	}
});

ItemSchema.plugin(idvalidator);

ItemSchema.plugin(require('mongoose-autopopulate'));

ItemSchema.methods.toDTO = function () {
	let filhosDTO = [];

	if (this.ItensFilhos.length > 0) {
		let letAux;
		for (letAux = 0; letAux < this.ItensFilhos.length; letAux++) {
			filhosDTO.push(this.ItensFilhos[letAux].toDTO());
		}
	}
	return new itemDTO(this._id, this.id_produto, this.id_material, this.id_acabamento, filhosDTO, this.Altura, this.Largura, this.Profundidade,this.Preco);
}


ItemSchema.methods.apagarItem = function (idItemFilho){
	let i;
	let novaLista = [];
	for(i=0;i<this.ItensFilhos.length;i++){
		if(this.ItensFilhos[i]._id!=idItemFilho){
			novaLista.push(this.ItensFilhos[i]);
		}
	}
	this.ItensFilhos = novaLista;
	return true;
}

module.exports = mongoose.model('Item', ItemSchema);

