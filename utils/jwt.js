const expressJwt = require('express-jwt');
const config = require('../config.json');

module.exports = jwt;

function jwt() {
    const { secret } = config;
    return expressJwt({ secret }).unless({
        path: [{
            url: '/cidade', methods: ['GET']
        }, {
            url: '/item', methods: ['GET'],
        },
        {
            url: '/encomenda',  methods: ['GET']
        },
        {
            url: '/item/itensNaoEncomendados',  methods: ['GET']
        }
        ]
    });
}