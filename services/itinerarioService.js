const URL_ITINERARIO = 'http://23.100.14.111:443/getIt?lista=';
const itinerario = require('../models/itinerario');
const fabrica = require('../models/fabrica');
const cidade = require('../models/cidade');
const encomenda = require('../models/encomenda');
const encomendaService = require('../services/encomendaService');
var service = new encomendaService();
var estadosDasEncomendas = require('../models/encomenda').estados;
var tarefa = require('node-schedule');
const TEMPO_ESPERA = 30; // em minutos

class itinerarioService {
    constructor() { }

    async gerarItinerarioFabricas() {
        let listaFabricas = await fabrica.find({}).exec(); // buscar todas as fábricas
        let i;
        let encomendas = [];
        const pedidoHTTP = require('request');
        for (i = 0; i < listaFabricas.length; i++) {
            let url = URL_ITINERARIO;
            let fabricaAtual = listaFabricas[i];
            let cidadesIterar = await service.cidadesIterarFabrica(fabricaAtual._id);
            if (cidadesIterar.length == 0) continue; // não há necessidade de gerar itinerários para fábricas sem encomendas
            let listaEncomendasDaFabrica = await service.encomendasFabrica(fabricaAtual._id);
            for (let j = 0; j < listaEncomendasDaFabrica.length; j++) {
                encomendas.push(listaEncomendasDaFabrica[j]);
            }
            url += fabricaAtual.Cidade.Nome + ',' + fabricaAtual.Cidade.Latitude + ',' + fabricaAtual.Cidade.Longitude;
            let j;
            for (j = 0; j < cidadesIterar.length; j++) {
                let cidadeAtual = cidadesIterar[j];
                url += '&lista=' + cidadeAtual.Nome + ',' + cidadeAtual.Latitude + ',' + cidadeAtual.Longitude;
            }
            pedidoHTTP(url, async function (error, response, body) {
                body = body.replace(/[\n\r]/g, '');
                body = body.substring(1, body.length - 1); // retirar os parênteses retos
                body = body.split(',');
                let referenciasCidades = [];
                let k;
                for (k = 0; k < body.length; k++) {
                    let cidadeAtual = await cidade.findOne({ Nome: body[k] }).exec();
                    referenciasCidades.push(cidadeAtual._id); // colocar a referência no vetor
                }
                let novoItinerario = new itinerario({
                    Fabrica: fabricaAtual._id,
                    Percurso: referenciasCidades
                });
                novoItinerario.save(async function (err) {
                    let i;
                    for (i = 0; i < listaEncomendasDaFabrica.length; i++) {
                        await encomenda.findByIdAndUpdate(listaEncomendasDaFabrica[i]._id,
                            {
                                EstadoEncomenda: estadosDasEncomendas.EXPEDIDA
                            }, function (err) {
                                if (err) {
                                    res.send("Erro ao atualizar - verificar integridade dos dados!");
                                }
                            });
                    }
                    if (err) res.send("Erro ao guardar");
                });
            });
        }

        for (let i = 0; i < encomendas.length; i++) {
            let dataAtual = new Date();
            let dataTarefa = new Date(dataAtual.getTime() + TEMPO_ESPERA * 60000);
            tarefa.scheduleJob(dataTarefa, () => {
                service.entregarEncomenda(encomendas[i]);
            });
        }
    }
}

module.exports = itinerarioService;