var Client = require('node-rest-client').Client;
const item = require('../models/item');
const jwt = require('jsonwebtoken');
const config = require('../config.json');
const OFFSET_PROFUNDIDADE = 3;

class itemService {

	constructor() {
	}

	validarDimensoes(idProduto, altura, largura, profundidade) {
		const { secretCatalogos } = config;
		let token = jwt.sign({}, secretCatalogos, {
			expiresIn: 30 // expira em meio minuto
		});
		let autorizacao = 'Bearer ' + token;
		const pedidoHTTP = require('request');
		let url = 'https://sic-productions.azurewebsites.net/api/produto/verificaritem/'; // isto devia ser uma variável estática global
		let urlAux1 = url.concat(idProduto);
		let urlAux2 = urlAux1.concat('/');
		let urlAux3 = urlAux2.concat(altura);
		let urlAux4 = urlAux3.concat('/');
		let urlAux5 = urlAux4.concat(largura);
		let urlAux6 = urlAux5.concat('/');
		let urlFinal = urlAux6.concat(profundidade);
		return new Promise(function (resolve, reject) { // é necessário uma promessa de forma a garantir que espera pelo http GET que é assíncrono
			pedidoHTTP({
				headers: {
					'Authorization': autorizacao
				},
				uri: urlFinal
			}, function (error, response, body) {
				if (response.statusCode != 200) {
					reject('Dimensões não são compatíveis');
				} else {
					resolve(true);
				}
			});
		});
	}

	validarProdutoId(idProduto) {
		const { secretCatalogos } = config;
		let token = jwt.sign({}, secretCatalogos, {
			expiresIn: 30 // expira em meio minuto
		});
		let autorizacao = 'Bearer ' + token;
		const pedidoHTTP = require('request');
		let url = 'https://sic-productions.azurewebsites.net/api/produto' + '/' + idProduto;
		return new Promise(function (resolve, reject) {
			pedidoHTTP({
				headers: {
					'Authorization': autorizacao
				},
				uri: url
			}, function (error, response, body) {
				if (response.statusCode == 404) {
					reject('O produto escolhido não existe.');
				} else {
					resolve(true);
				}
			});
		});
	}

	validarMaterialAcabamento(idProduto, idMaterial, idAcabamento) {
		const { secretCatalogos } = config;
		let token = jwt.sign({}, secretCatalogos, {
			expiresIn: 30 // expira em meio minuto
		});
		let autorizacao = 'Bearer ' + token;
		const pedidoHTTP = require('request');
		let url = 'https://sic-productions.azurewebsites.net/api/produto/verificarItem/'; // isto devia ser uma variável estática global
		let urlAux1 = url.concat(idProduto);
		let urlAux2 = urlAux1.concat('/');
		let urlAux3 = urlAux2.concat(idMaterial);
		let urlAux4 = urlAux3.concat('/');
		let urlFinal = urlAux4.concat(idAcabamento);
		return new Promise(function (resolve, reject) { // é necessário uma promessa de forma a garantir que espera pelo http GET que é assíncrono
			pedidoHTTP({
				headers: {
					'Authorization': autorizacao
				},
				uri: urlFinal
			}, function (error, response, body) {
				if (response.statusCode == 200) {
					resolve(true);
				} else {
					reject('Material e Acabamento não existem');
				}
			});
		});
	}

	async apagarReferencias(idItemApagado) {
		let listaItens = await item.find({}).exec();
		let i;
		for (i = 0; i < listaItens.length; i++) {
			let retorno = await listaItens[i].apagarItem(idItemApagado);
			const updatedPlot = await item.findByIdAndUpdate(
				listaItens[i]._id,
				{
					$set: {
						ItensFilhos: listaItens[i].ItensFilhos
					}
				},
				{ new: true }
			);
		}

	}

	async validarItensFilhos(idProdutoPai, idsProdutosFilhos) {
		const { secretCatalogos } = config;
		let token = jwt.sign({}, secretCatalogos, {
			expiresIn: 30 // expira em meio minuto
		});
		let autorizacao = 'Bearer ' + token;
		let urlPost = "https://sic-productions.azurewebsites.net/api/produto/itensfilhos"
		let clienteHttp = new Client();
		let idsFilhos = [];
		let i;
		for (i = 0; i < idsProdutosFilhos.length; i++) {
			let itemPreenchido = await item.findOne({ _id: idsProdutosFilhos[i] }).exec();
			idsFilhos.push(itemPreenchido.id_produto);
		}
		let args = {
			data: {
				"ProdutoPaiId": idProdutoPai,
				"ProdutosFilhosIds": idsFilhos // [1,2,4,5]
			},
			headers: {
				"Content-Type": "application/json",
				"Authorization": autorizacao
			}
		}


		return new Promise(function (resolve, reject) {
			clienteHttp.post(urlPost, args, function (data, response) {
				if (response.statusCode == 401) {
					reject('Não tem permissões');
					return;
				}
				if (data) {
					resolve(true);
				} else {
					reject('Um dos itens filhos não pertence ao item pai');
				}
			});
		});
	}

	async buscarTodosItensPai(idItem) {
		let listaPais = [];
		let aux1 = String(idItem);
		let todosItens = await item.find({}).exec();
		let i;
		for (i = 0; i < todosItens.length; i++) { 						// percorrer cada item existente
			let itemAtual = todosItens[i];
			let aux2 = String(itemAtual._id);
			if (aux1 != aux2) { 										// para não percorrer o próprio item recebido como parâmetro
				let j; 													
				for (j = 0; j < itemAtual.ItensFilhos.length; j++) {	// agora percorrer todos os filhos de cada item
					let itemAtualFilho = itemAtual.ItensFilhos[j];
					let aux3 = String(itemAtualFilho._id);
					if (aux1 == aux3) {									 // se for um item filho do itemAtual
						listaPais.push(itemAtual);
					}
				}
			}
		}
		return listaPais;
	}

	async validarDimensoesFilhos(alturaPai, larguraPai, profundidadePai, idsProdutosFilhos) {
		let i;
		for (i = 0; i < idsProdutosFilhos.length; i++) {
			let itemFilhoAtual = await item.findOne({ _id: idsProdutosFilhos[i] }).exec();
			if (itemFilhoAtual.Largura >= larguraPai || itemFilhoAtual.Altura >= alturaPai
				|| itemFilhoAtual.Profundidade >= profundidadePai) {
				return false;
			}
		}
		return true;
	}
	// comecou aqui

	async adicionarItemFilho(itemPai, itemFilho) {
		//if (this.tipo != 0) return false; // não adiciona se o item pai não for armário
		if (itemFilho.Largura >= itemPai.Largura) return false;
		if (itemFilho.Altura >= itemPai.Altura) return false;
		if (itemFilho.Profundidade >= itemPai.Profundidade - OFFSET_PROFUNDIDADE) return false;
		itemPai.ItensFilhos.push(itemFilho);
		let retorno = await this.calcularPosicaoFilhos(itemPai);
		if (!retorno) {
			//this.itensFilhos.splice(this.itensFilhos.length - 1, 1);
			return false;
		}

		return true;
	}

	espessura(itemRecebido) {
		let x, resultado;
		if (itemRecebido.Largura > itemRecebido.Altura) x = itemRecebido.Largura;
		else x = itemRecebido.Altura;
		resultado = Math.floor(x / 100);
		if (resultado < 2) return 2;
		return resultado;
	}

	async calcularPosicaoFilhos(itemPai) {
		if (itemPai.ItensFilhos.length == 0) return true; // não faz nada se não houver filhos
		let espessuraPai = this.espessura(itemPai);
		// constroi uma matriz de booleans a false, do tamanho do item
		let i, j, matriz = [];
		for (i = 0; i < itemPai.Altura; i++) {
			matriz[i] = [];
			for (j = 0; j < itemPai.Largura; j++) {
				if (i < espessuraPai || j < espessuraPai || i >= itemPai.Altura - espessuraPai || j >= itemPai.Largura - espessuraPai) { // para evitar colisões entre meshs das paredes
					matriz[i][j] = true;
				} else {
					matriz[i][j] = false;
				}
			}
		}

		// clona a lista de items filho para uma lista à parte, para poder ser manipulada
		let itensFilhosClone = [];
		for (i = 0; i < itemPai.ItensFilhos.length; i++) {
			itensFilhosClone.push(itemPai.ItensFilhos[i]);
		}

		while (itensFilhosClone.length > 0) {
			// procura o filho com maior área - TODO: arranjar maneira de verificar variações nesta ordem - se falhar à primeira, testa com outras ordens
			let maxArea = 0, indexMaxArea, currArea;
			for (i = 0; i < itensFilhosClone.length; i++) {
				currArea = itensFilhosClone[i].Altura * itensFilhosClone[i].Largura;
				if (currArea > maxArea) {
					maxArea = currArea;
					indexMaxArea = i;
				}
			}
			// se o item não encaixa, retorna sem fazer alterações
			if (!this.encaixa(itensFilhosClone[indexMaxArea], matriz)) return false;
			itensFilhosClone.splice(indexMaxArea, 1);
		}

		return true;
	}

	// verifica se existe espaço suficiente para encaixar o item, tendo em conta a sua largura e altura
	espacoLivre(matriz, i, j, altura, largura) {
		let x, y;
		for (x = i; x < i + altura; x++) {
			for (y = j; y < j + largura; y++) {
				if (matriz[x][y]) return false;
			}
		}
		return true;
	}

	// 'encaixa' o item, ao meter a true o espaço correspondente
	encaixaAux(matriz, i, j, altura, largura) {
		let x, y;
		for (x = i; x < i + altura; x++) {
			for (y = j; y < j + largura; y++) {
				matriz[x][y] = true;
			}
		}
	}

	encaixa(item, matriz) {
		let i, j;
		for (i = 0; i < matriz.length; i++) {
			for (j = 0; j < matriz[0].length; j++) {
				if (!matriz[i][j]) { // se o lugar estiver desocupado
					if (j + item.Largura < matriz[0].length && i + item.Altura < matriz.length) { // se ainda couber, numericamente, no espaço restante
						if (this.espacoLivre(matriz, i, j, item.Altura, item.Largura)) {
							item.posicaoTempX = j; // guarda os valores no atributo temporário da classe
							item.posicaoTempY = i;
							this.encaixaAux(matriz, i, j, item.Altura, item.Largura);
							return true;
						}
					}
				}
			}
		}

		return false;
	}

}

module.exports = itemService;
