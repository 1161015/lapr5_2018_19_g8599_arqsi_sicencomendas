var Client = require('node-rest-client').Client;
const item = require('../models/item');
const encomenda = require('../models/encomenda');
var estadosDasEncomendas = require('../models/encomenda').estados;
var tarefa = require('node-schedule');
const jwt = require('jsonwebtoken');
const config = require('../config.json');
const URL_FABRICA = 'http://23.100.14.111:443/getFac?lista=';

const cidade = require('../models/cidade');
const fabrica = require('../models/fabrica');

const TEMPO_ESPERA = 5; // em minutos
class encomendaService {

	constructor() { }



	async validarObrigatoriedade(idItemPai) {
		const { secretCatalogos } = config;
		let token = jwt.sign({}, secretCatalogos, {
			expiresIn: 30 // expira em meio minuto
		});
		let autorizacao = 'Bearer ' + token;
		let ItemPai = await item.findOne({ _id: idItemPai }).exec();
		let cliente = new Client();
		let urlPost = "https://sic-productions.azurewebsites.net/api/produto/obrigatoriedade";
		let idsFilhos = [];
		let i;
		for (i = 0; i < ItemPai.ItensFilhos.length; i++) {
			let itemPreenchido = await item.findOne({ _id: ItemPai.ItensFilhos[i] }).exec();
			idsFilhos.push(itemPreenchido.id_produto);
		}
		let args = {
			data: {
				"ProdutoPaiId": ItemPai.id_produto,
				"ProdutosFilhosIds": idsFilhos // [1,2,4,5]
			},
			headers: {
				"Content-Type": "application/json",
				"Authorization": autorizacao
			}
		}
		return new Promise(function (resolve, reject) {
			cliente.post(urlPost, args, function (data, response) {
				if (response.statusCode == 401) {
					reject('Não tem permissões');
					return;
				}
				if (data) {
					resolve(true)
				} else {
					reject('Não tem todos os itens obrigatórios.')
				}

			});
		});
	}

	// "ProdutoPaiId":22,
	// "MaterialAcabamentoPai":{"Chave":3,"Valor":3},
	// "MaterialAcabamentoIds":[{"Chave":3,"Valor":3}]

	async validarMaterialAcabamento(itemRecebido) {
		const { secretCatalogos } = config;
		let token = jwt.sign({}, secretCatalogos, {
			expiresIn: 30 // expira em meio minuto
		});
		let autorizacao = 'Bearer ' + token;
		let ItemPai = await item.findOne({ _id: itemRecebido }).exec();

		let cliente = new Client();
		let urlPost = "https://sic-productions.azurewebsites.net/api/produto/materialacabamento";
		let idsMateriais = [];
		let produtosFilhosIds = [];
		let i;
		for (i = 0; i < ItemPai.ItensFilhos.length; i++) {
			let itemPreenchido = await item.findOne({ _id: ItemPai.ItensFilhos[i] }).exec();
			produtosFilhosIds.push(itemPreenchido.id_produto);
			idsMateriais.push(itemPreenchido.id_material);
		}
		let args = {
			data: {
				"ProdutoPaiId": ItemPai.id_produto,
				"ProdutosFilhosIds": produtosFilhosIds,
				"MaterialAcabamentoPai": {
					"Chave": ItemPai.id_material
				},
				"MaterialIdFilhos": idsMateriais

			},
			headers: {
				"Content-Type": "application/json",
				"Authorization": autorizacao
			}
		}
		return new Promise(function (resolve, reject) {
			cliente.post(urlPost, args, function (data, response) {
				if (response.statusCode == 401) {
					reject('Não tem permissões');
					return;
				}
				if (data) {
					resolve(true)
				} else {
					reject('Os itens filhos não respeitam as restrições de material do pai.')
				}

			});
		});

	}

	// verifica se a percentagem volumica que os filhos de um item ocupam em relação ao mesmo encaixam nos limites da restrição do produto a que o item se refere
	async validarTaxaOcupacao(itemRecebido) {
		const { secretCatalogos } = config;
		let token = jwt.sign({}, secretCatalogos, {
			expiresIn: 30 // expira em meio minuto
		});
		let autorizacao = 'Bearer ' + token;
		let ItemPai = await item.findOne({ _id: itemRecebido }).exec();
		let cliente = new Client();
		let urlPost = "https://sic-productions.azurewebsites.net/api/produto/taxapercentual";
		let produtosFilhosIds = [];
		let percentagensFilhos = [];
		let i;
		for (i = 0; i < ItemPai.ItensFilhos.length; i++) {
			let itemPreenchido = await item.findOne({ _id: ItemPai.ItensFilhos[i] }).exec();
			produtosFilhosIds.push(itemPreenchido.id_produto);
			let percentagemAltura = itemPreenchido.Altura / ItemPai.Altura * 100;
			let percentagemLargura = itemPreenchido.Largura / ItemPai.Largura * 100;
			let percentagemProfundidade = itemPreenchido.Profundidade / ItemPai.Profundidade * 100;
			percentagensFilhos.push({
				"PercentagemAltura": percentagemAltura,
				"PercentagemLargura": percentagemLargura,
				"PercentagemProfundidade": percentagemProfundidade
			});
		}
		let args = {
			data: {
				"ProdutoPaiId": ItemPai.id_produto,
				"ProdutosFilhosIds": produtosFilhosIds,
				"PercentagensFilhos": percentagensFilhos
			},
			headers: {
				"Content-Type": "application/json",
				"Authorization": autorizacao
			}
		}
		return new Promise(function (resolve, reject) {
			cliente.post(urlPost, args, function (data, response) {
				if (response.statusCode == 401) {
					reject('Não tem permissões');
					return;
				}
				if (data) {
					resolve(true)
				} else {
					reject('Verifique as taxas de ocupação dos itens filhos.')
				}
			});
		});
	}

	async itemPresenteEncomenda(idItemRecebido) {
		let todasEncomendas = await encomenda.find({}).exec();
		let i;
		for (i = 0; i < todasEncomendas.length; i++) {
			let encomendaAtual = todasEncomendas[i];
			let todosItensEncomenda = [];
			let j;
			for (j = 0; j < encomendaAtual.Itens.length; j++) {
				let itemAtual = encomendaAtual.Itens[j];
				this.preencherTodosItensEncomendaRecursivamente(todosItensEncomenda, itemAtual);
			}
			let k;
			for (k = 0; k < todosItensEncomenda.length; k++) {
				let aux = String(todosItensEncomenda[k]._id);
				let aux1 =  String(idItemRecebido);
				if(aux===aux1){
					return true;
				}
			}
		}
		return false;
	}

	preencherTodosItensEncomendaRecursivamente(array, item) {
		array.push(item);
		let i;
		for (i = 0; i < item.ItensFilhos.length; i++) {
			this.preencherTodosItensEncomendaRecursivamente(array, item.ItensFilhos[i]);
		}
	}



	validarEncomenda(encomendaRecebida) {
		encomenda.findByIdAndUpdate(encomendaRecebida._id,
			{
				EstadoEncomenda: estadosDasEncomendas.VALIDADA
			}, function (err) {
				if (err) {
					res.send("Erro ao atualizar - verificar integridade dos dados!");
				} else {
					let dataAtual = new Date();
					let dataTarefa = new Date(dataAtual.getTime() + TEMPO_ESPERA * 60000);
					tarefa.scheduleJob(dataTarefa, function () {
						let servico = new encomendaService();
						servico.assignarEncomenda(encomendaRecebida);
					});
				}
			});
	}

	async assignarEncomenda(encomendaRecebida) {
		let urlPedido = "";
		let cidadeEncomenda = await cidade.findOne({ _id: encomendaRecebida.Morada }).exec();
		urlPedido = URL_FABRICA + cidadeEncomenda.Nome + ',' + cidadeEncomenda.Latitude + ','
			+ cidadeEncomenda.Longitude; // sem a lista
		let todasFabricas = await fabrica.find({}).exec();
		let i;
		for (i = 0; i < todasFabricas.length; i++) {
			urlPedido += '&lista=' + todasFabricas[i].Cidade.Nome + ',' + todasFabricas[i].Cidade.Latitude + ',' + todasFabricas[i].Cidade.Longitude;
		}
		const pedidoHTTP = require('request');
		pedidoHTTP(urlPedido, async function (error, response, body) {
			body = body.replace(/[\n\r]/g, ''); // /g para ser global, \n e \r para remover newline e o carriage return
			let cidadePorNome = await cidade.findOne({ Nome: body }).exec();
			let fabricaAtribuida = await fabrica.findOne({ Cidade: cidadePorNome._id }).exec();
			encomenda.findByIdAndUpdate(encomendaRecebida._id,
				{
					EstadoEncomenda: estadosDasEncomendas.ASSIGNADA,
					FabricaAtribuida: fabricaAtribuida._id
				}, function (err) {
					if (err) {
						res.send("Erro ao atualizar - verificar integridade dos dados!");
					} else {
						let dataAtual = new Date();
						let dataTarefa = new Date(dataAtual.getTime() + TEMPO_ESPERA * 60000);
						tarefa.scheduleJob(dataTarefa, () => {
							let servico = new encomendaService();
							servico.produzirEncomenda(encomendaRecebida);
						});
					}
				});
		});
	}

	produzirEncomenda(encomendaRecebida) {
		encomenda.findByIdAndUpdate(encomendaRecebida._id,
			{
				EstadoEncomenda: estadosDasEncomendas.EM_PRODUCAO
			}, function (err) {
				if (err) {
					res.send("Erro ao atualizar - verificar integridade dos dados!");
				} else {
					let dataAtual = new Date();
					let dataTarefa = new Date(dataAtual.getTime() + TEMPO_ESPERA * 60000);
					tarefa.scheduleJob(dataTarefa, () => {
						let servico = new encomendaService();
						servico.embalarEncomenda(encomendaRecebida);
					});
				}
			});
	}

	embalarEncomenda(encomendaRecebida) {
		encomenda.findByIdAndUpdate(encomendaRecebida._id,
			{
				EstadoEncomenda: estadosDasEncomendas.EM_EMBALAMENTO
			}, function (err) {
				if (err) {
					res.send("Erro ao atualizar - verificar integridade dos dados!");
				} else {
					let dataAtual = new Date();
					let dataTarefa = new Date(dataAtual.getTime() + TEMPO_ESPERA * 60000);
					tarefa.scheduleJob(dataTarefa, () => {
						let servico = new encomendaService();
						servico.prontaExpedir(encomendaRecebida);
					});
				}
			});
	}

	prontaExpedir(encomendaRecebida) {
		encomenda.findByIdAndUpdate(encomendaRecebida._id,
			{
				EstadoEncomenda: estadosDasEncomendas.PRONTA_EXPEDIR
			}, function (err) {
				if (err) {
					res.send("Erro ao atualizar - verificar integridade dos dados!");
				}
			});
	}


	entregarEncomenda(encomendaRecebida) {
		encomenda.findByIdAndUpdate(encomendaRecebida._id,
			{
				EstadoEncomenda: estadosDasEncomendas.ENTREGUE
			}, function (err) {
				if (err) {
					res.send("Erro ao atualizar - verificar integridade dos dados!");
				}
			});
	}

	async cidadesIterarFabrica(idFabrica) {
		let encomendasDaFabrica = await this.encomendasFabrica(idFabrica);
		if (encomendasDaFabrica.length == 0) return []; // não existe encomendas para a fábrica retorna logo vetor vazio
		let cidadesNaoRepetidas = [];
		encomendasDaFabrica.forEach(encomendaAtual => {
			if (cidadesNaoRepetidas.indexOf(encomendaAtual.Morada) === -1) { // basicamente é um !contains
				cidadesNaoRepetidas.push(encomendaAtual.Morada);
			}
		});
		return cidadesNaoRepetidas;
	}

	async encomendasFabrica(idFabrica) {
		let encomendasDaFabrica = await encomenda.find({
			FabricaAtribuida: idFabrica,
			EstadoEncomenda: estadosDasEncomendas.PRONTA_EXPEDIR
		}).exec();
		return encomendasDaFabrica;
	}


}

module.exports = encomendaService;