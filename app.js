const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const cors = require('cors');
const jwt = require('./utils/jwt');
const errorHandler = require('./utils/errorHandler');
const itinerarioService = require('./services/itinerarioService');
var schedule = require('node-schedule');

let dev_db_url = 'mongodb://Admin:Facil123@ds159782.mlab.com:59782/sic_encomendas';
let mongoDB = process.env.MONGODB_URI || dev_db_url;
mongoose.connect(mongoDB);
mongoose.Promise = global.Promise;
let db = mongoose.connection;
db.on('error', console.error.bind('MongoDB connection error:'));
// initialize our express app
const encomenda = require('./routes/encomendaRoute');
const item = require('./routes/itemRoute');
const cidade = require('./routes/cidadeRoute');
const fabrica = require('./routes/fabricaRoute');
const app = express();
app.use(cors());
// tokens
app.use(jwt());
app.use(errorHandler);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:false}));
app.use('/encomenda', encomenda);
app.use('/item', item);
app.use('/cidade', cidade);
app.use('/fabrica', fabrica);
/**
 * Todos os domingos vai gerar os itinerários das fábricas domingo = 0 , à meia noite
 */
schedule.scheduleJob('0 0 0 * * 0', function(){
    let iService =  new itinerarioService();
    iService.gerarItinerarioFabricas();
})

let port = process.env.PORT || 8080;

app.listen(port, () => {
    console.log('Server is up and running on port number ' + port);
});